class PostsController < ApplicationController
  include PostsHelper
  before_action :authenticate_user!, except: [:index, :show, :search]
  before_action :set_post, except: [:index, :new, :create, :search]
  before_action :user_correct, except: [:index, :show, :new, :create, :search]

  def search
    if params[:search].present?
      @posts = Post.search_by_title(params[:search]).page(params[:page]).per(6).order("created_at DESC").with_attached_image
      render 'index'
    else
      @posts = Post.page(params[:page]).per(6).order("created_at DESC").with_attached_image
      render 'index'
    end
  end

  def index
    if params[:tag_selected].present?
      @posts = Post.tagged_with(params[:tag_selected], any: true).page(params[:page]).per(6).order("created_at DESC").with_attached_image
    else
      @posts = Post.page(params[:page]).per(6).order("created_at DESC").with_attached_image
      #with_attached_image 避免ActiveStorage n+1問題
    end
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = '已發布!'
      redirect_to @post
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @post.update_attributes(post_params)
      flash[:success] = '已編輯!'
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:danger] = '已刪除!'
    redirect_to posts_url
  end

  private
    def post_params
      params.require(:post).permit(:title, :brief, :content, :image, :tag_list)
    end
    def set_post
      @post = Post.friendly.find(params[:id])
    end
    def user_correct
      unless post_owner?
        flash[:danger] = '沒有權限'
        redirect_to root_url
      end
    end
end
