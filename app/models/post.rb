class Post < ApplicationRecord
  include PgSearch
  pg_search_scope :search_by_title,
                   against: :title,
                   using: {
                    tsearch: {prefix: true}
                   }
  extend FriendlyId
  friendly_id :title, use: :slugged

  def normalize_friendly_id(input)
    input.to_s.to_slug.normalize.to_s
  end


  has_one_attached :image

  acts_as_taggable # Alias for acts_as_taggable_on :tags

  belongs_to :user
  validates :title, presence: {message: '必須有標題'}
  validates :content, presence: {message: '必須有內容'}
  validates :brief, presence: {message: '必須有概要'}
  validate :correct_image_type

  private
    def correct_image_type
      if !image.attached?
        errors.add(:image, '必須有封面')
      elsif !image.content_type.in?(%w(image/jpeg image/png ))
        errors.add(:image, '圖片格式須為(jpeg/png)')
      end
    end
end
