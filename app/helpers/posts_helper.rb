module PostsHelper
  def post_owner?
    @post.user.email == current_user.email
  end
end
