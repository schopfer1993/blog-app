class AddBriefToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :brief, :text
  end
end
