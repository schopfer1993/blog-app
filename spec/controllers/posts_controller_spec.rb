require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  before(:all) do
    @user = User.create(email: 'user@name.com', password: 'password',
                        password_confirmation: 'password')
    @post = @user.posts.create(title: 'foo', content: 'bar', brief: 'parapara')
  end

  it "index動作沒有問題" do
    get :index
    expect(response).to have_http_status(:ok)
    expect(response).to render_template('index')
  end

  it "new動作沒有問題" do
    sign_in @user
    get :new
    expect(response).to have_http_status(:ok)
    expect(response).to render_template('new')
  end

  it "show動作沒有問題" do
    get :show, params:{id: @post.id}
    expect(response).to have_http_status(:ok)
    expect(response).to render_template('show')
  end

  it "edit動作沒有問題" do
    sign_in @user
    get :edit, params:{id: @post.id}
    expect(response).to have_http_status(:ok)
    expect(response).to render_template('edit')
  end
end
