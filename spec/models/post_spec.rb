require 'rails_helper'

RSpec.describe Post, type: :model do

  before(:all) do
    @post = Post.create(title: 'foo', content: 'bar', brief: 'parapara')
  end

  it "title不能空白" do
    @post.title = ''
    expect(@post).to be_invalid
  end

  it "content不能空白" do
    @post.content = ''
    expect(@post).to be_invalid
  end

  it "brief不能空白" do
    @post.brief = ''
    expect(@post).to be_invalid
  end
end
